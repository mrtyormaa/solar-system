# R Demo Repository

[![pipeline status](https://gitlab.com/mrtyormaa/solar-system/badges/master/pipeline.svg)](https://gitlab.com/mrtyormaa/solar-system/badges/master)
[![coverage report](https://gitlab.com/mrtyormaa/solar-system/badges/master/coverage.svg)](https://gitlab.com/mrtyormaa/solar-system/badges/master)
[![CRAN version](http://www.r-pkg.org/badges/version/covr)](https://cran.r-project.org/package=covr)

# Contact
Point of contact in case of queries regarding the project
* [Asutosh Satapathy](https://gitlab.com/mrtyormaa) - asa@ase.ch
